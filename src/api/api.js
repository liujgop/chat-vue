import axios from 'axios'
//获取emoji头像(本地json只能放在static文件夹里)
export function getEmojiData() {
  return axios({
      method: 'get',
      url: '/static/emojiDB.json',
    })
    .then(function(res) {
      return Promise.resolve(res.data);
    });
}

export function sendMsg(data) {
     console.log("data",data);
  return axios({
    method: 'post',
    url: 'https://chat.luozejiao.com/api/message/send',
    params:data,
  })
    .then(function(res) {
         console.log("res",res);
      return Promise.resolve(res);
    });
}


export function register(data) {
  console.log("data",data);
  return axios({
    method: 'post',
    url: 'http://127.0.0.1:8080/api/register',
    data:data,
  })
    .then(function(res) {
      console.log("res",res);
      return Promise.resolve(res);
    });
}

export function login(data) {
  console.log("data",data);
  return axios({
    method: 'post',
    url: 'http://127.0.0.1:8080/api/login',
    data:data,
  })
    .then(function(res) {
      console.log("res",res);
      return Promise.resolve(res);
    });
}
